import { combineReducers } from 'redux';
import AppListReducer from './appListReducer';
import dictionaryReducer from './dictionaryReducer';

export default combineReducers({
    appList: AppListReducer,
    dictionary: dictionaryReducer
});

