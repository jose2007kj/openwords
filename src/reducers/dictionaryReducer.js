import {START_FETCHING_WORD,FETCHING_WORD_SUCCESSFULL,FETCHING_WORD_FAILED} from '../actions/types';
const INITIAL_STATE = {
    word: '',
    fetch_status:'',
    error:''
}
export default (state=INITIAL_STATE,action)=>{
    switch(action.type){
        case START_FETCHING_WORD:
            return{
                ...state,
                fetch_status:"loading"
            }
        case FETCHING_WORD_FAILED:
            return{
                ...state,
                word:'',
                error:action.payload,
                fetch_status:"failed"
            }
        case FETCHING_WORD_SUCCESSFULL:
            return{
                ...state,
                word:action.payload['word']+":"+action.payload['meaning'],
                fetch_status: 'success'
            }
        default:
            return state;
    }
}

