import React, { useState, useEffect } from 'react';
import { Dimensions,StyleSheet, Text, View,NativeModules,Image, TouchableOpacity,ScrollView, ActivityIndicator,AppState } from 'react-native';
import {fetchWords,fetchInstalledApps,openSettingsPage,openApp,openDialerPage,openCameraPage} from '../actions';
import { useDispatch, useSelector } from "react-redux";
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/Feather';
var width = Dimensions.get('window').width;
var height=Dimensions.get('window').height;
function HomeScreen() {
    const [appstate,setAppstate] = useState(AppState.currentState)
    const fetch_status_apps = useSelector(state => state.appList.fetch_status)
    const appList = useSelector(state => state.appList.appList)
    const applist_error = useSelector(state => state.appList.error)
    const word = useSelector(state => state.dictionary.word)
    const fetch_status_verse = useSelector(state => state.dictionary.fetch_status)
    const dispatch = useDispatch();

    // componendidupdate
    useEffect(() => {
            dispatch(fetchWords());
            dispatch(fetchInstalledApps());
            AppState.addEventListener('focus', _handleAppStateChange);
    },[]);
    //componentwill unmount
    useEffect(()=> {
        return () => {
            AppState.removeEventListener('focus', _handleAppStateChange);
        };
    },[]);

    const _handleAppStateChange = (nextAppState) => {
        console.log('App has come to the foreground!');
        dispatch(fetchInstalledApps());
     };
    switch(fetch_status_apps){
         case 'failed':
                console.log("failed!!")
                return (
                <View style={styles.outer}>
                    <ActivityIndicator size={'large'} />
                </View>
                )
        default:
            return (
        <View style={styles.outer}>
        <View style={{ alignItems:'center', justifyContent:'center',width:width*0.9, height:height*0.35,marginRight:width*0.01,marginLeft:width*0.01, borderRadius:35, borderColor:'#000000', borderWidth:3, backgroundColor:'white'}}><ScrollView style={styles.scrollView}><Text style={styles.verse}>{word}</Text></ScrollView>

    <Button title="View another"
    onPress={() => dispatch(fetchWords())}
             buttonStyle={{backgroundColor: "#535353",
      width: width*0.4,
      borderColor: "transparent",
      borderWidth: 0,
      borderRadius: 20}}
      />
</View>
        <ScrollView style={styles.container}>
                {appList ? appList.map((app,i)=>{return(
                    <TouchableOpacity key={i} onPress={()=>dispatch(openApp(app))}
                    onLongPress={()=>dispatch(openSettingsPage(app.name))}
                    style={styles.appListItem}>

                        <Image style={styles.icon} resizeMode={'contain'} source={{uri: "data:image/png;base64," + app.icon}} />
                        <Text style={styles.text}>{app.label}</Text>

                    </TouchableOpacity>
                )}) :null}

            </ScrollView>
            <View style={styles.bottomOverlay}>
            <View style={styles.bottomTiles}>
            <Icon.Button name="phone" size={30} backgroundColor="#000000" type='feather' color="#FFFFFF" onPress={() => dispatch(openDialerPage())}/>
            </View>
            <View style={styles.bottomTiles}>
            <Text style={styles.text}></Text></View>
            <View style={styles.bottomTiles}>
            <Icon.Button  name="camera" type='feather' size={30} backgroundColor="#000000" color="#FFFFFF" onPress={() => dispatch(openCameraPage())}/>
            </View></View>
            </View>
        )
                   }
    }


/**
 * special styles for this page
 */
const styles = StyleSheet.create({
container: {
backgroundColor: '#000000',
    marginBottom: '5%'
},
    text: {
        color: 'white',
        fontSize: 20
    },
    icon: {
        marginRight: 16,
        width: 40,
        height: 40
    },
    appListItem: {
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    outer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
        backgroundColor: '#000000'


    },
    verse: {
        marginTop: 10,
        fontFamily: 'serif',
        fontSize: 15,
        fontWeight: 'bold',
        textAlign: "center",
        color:"#000000",
  textAlignVertical: "center",
      },
    buttonView:{
      marginTop:height*0.001,
      marginLeft:width*0.5
    },
    bottomOverlay:{
    height: '10%',
    width: width,
     flexDirection: 'row'
    },
    bottomTiles:{
        marginTop:'1%',
        flex: 1, alignItems: 'center'
    },
    scrollView: {
    marginHorizontal: 20,
    marginVertical:20
  }
});
export default HomeScreen;

