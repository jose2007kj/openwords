export const NAVIGATE_TO_APPS = "NAVIGATE_TO_APPS";
export const NAVIGATE_TO_HOME = "NAVIGATE_TO_HOME";
export const NAVIGATE_TO_SETTINGS = "NAVIGATE_TO_SETTINGS";

export const START_FETCHING_APPS = "START_FETCHING_APPS";
export const FETCHING_APPS_SUCCESSFULL = "FETCHING_APPS_SUCCESSFULL";
export const FETCHING_APPS_FAILED = "FETCHING_APPS_FAILED";

export const START_FETCHING_WORD = "START_FETCHING_WORD";
export const FETCHING_WORD_SUCCESSFULL = "FETCHING_WORD_SUCCESSFULL";
export const FETCHING_WORD_FAILED = "FETCHING_WORD_FAILED";

export const OPEN_SETTINGS_PAGE = "OPEN_SETTINGS_PAGE";
export const OPEN_APP = "OPEN_APP";
export const OPEN_DIALER = "OPEN_DIALER";
export const OPEN_CAMERA = "OPEN_CAMERA";

