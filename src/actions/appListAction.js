import {START_FETCHING_APPS,FETCHING_APPS_SUCCESSFULL,FETCHING_APPS_FAILED,OPEN_SETTINGS_PAGE,OPEN_APP,OPEN_DIALER,OPEN_CAMERA} from './types';
import {NativeModules} from 'react-native';
import { orderBy } from 'lodash'
export const fetchInstalledApps = () =>{
    console.log("fetch insalled apps")
    return async (dispatch) => {
        dispatch({
            type:START_FETCHING_APPS,
        })
            NativeModules.InstalledApps.getApps().then(data=>{
               let apps = JSON.parse(data['appList'])
                 apps = orderBy(apps,'label','asc');
                dispatch({
                type:FETCHING_APPS_SUCCESSFULL,
                payload: apps
                })

            }).catch(error=>{
            console.log("error fetching apps",error);
            dispatch({
                type:FETCHING_APPS_FAILED,
                payload: error
            })
        })
    }
}
export const openApp=(app)=>{

    return (dispatch) => {
        dispatch({
            type:OPEN_APP,
            payload: app.name
        })
    }
}

export const openSettingsPage = (app)=>{

    return (dispatch) => {
        dispatch({
            type:OPEN_SETTINGS_PAGE,
            payload:app
        })
    }
}
export const openDialerPage = () => {
    console.log("open dialer")
    return (dispatch) => {
        dispatch({
            type:OPEN_DIALER
        })
    }
}

export const openCameraPage = () => {
    console.log("open dialer camera")
    return (dispatch) => {
        dispatch({
            type:OPEN_CAMERA
        })
    }
}

