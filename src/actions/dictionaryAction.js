import * as words from '../assets/dictionary.json';
import {FETCHING_WORD_SUCCESSFULL,FETCHING_WORD_FAILED,START_FETCHING_WORD} from './types';

export const fetchWords =()=>{
    return async (dispatch)=>{
        dispatch({
            type: START_FETCHING_WORD
        })
        let keys = Object.keys(words);
        let e_word = keys[keys.length * Math.random() << 0]
        let e_meaning = words[e_word]

        try{
            dispatch({
                type: FETCHING_WORD_SUCCESSFULL,
                payload: {"word":e_word, "meaning": e_meaning}
            })
        }catch(e){
            dispatch({
                type: FETCHING_WORD_FAILED
            })
        }
    }
}

