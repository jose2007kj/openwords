import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware,compose} from 'redux';
import ReduxThunk from 'redux-thunk';
import RouterComponent from './Router';
import reducers from './reducers';
  const App = () => {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
        <Provider store={store}>
           <RouterComponent />
        </Provider>
    );
  }
export default App;
