import React from "react";
import { Router, Scene} from "react-native-router-flux";
import { StyleSheet,BackHandler } from "react-native";
import { useDispatch } from 'react-redux';
import {fetchInstalledApps} from './actions';
import HomeScreen from './components/homeScreen';
function RouterComponent() {
    const dispatch = useDispatch();
    onBackPressed = () => {
      console.log("no to CAB", Actions)
      return true
};
  onEnter=()=>{
   // this.props.fetchInstalledApps()
  }
  return (
    <Router
    >
      <Scene key="root" hideNavBar>
        <Scene
          key="home"
          hideNavBar
          component={HomeScreen}
          onEnter={() => dispatch(fetchInstalledApps())}
          initial
        />
      </Scene>
    </Router>
  );
};


export default RouterComponent;

